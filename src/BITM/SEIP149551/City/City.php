<?php

namespace App\City;
use App\Message\Message;

use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class City extends DB
{

    public $id;
    public $name;
    public $city;


    public function __construct()
    {

        parent::__construct();

    }
    
    

        public function setData($postVariableData=NULL)
    {
        if(array_key_exists('id',$postVariableData))
        {
            $this->id=$postVariableData['id'];
        }
        if(array_key_exists('name',$postVariableData))
        {
            $this->name=$postVariableData['name'];
        }
        if(array_key_exists('city',$postVariableData))
        {
            $this->city=$postVariableData['city'];
        }
    }
        public  function  store()
    {
        $arrData=array($this->name,$this->city);
        $sql="Insert INTO city(name,city) VALUE (?,?)";
        //var_dump($sql);

        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result)
            Message::setMessage("Success! Data has been inserted successfully:)");
        else
            Message::setMessage("Failed !Data has been inserted successfully:(");
        Utility::redirect('create.php');
    }//end of store m


    public function index($fetchMode='ASSOC'){
        $sql = "SELECT * from city where is_deleted = 0 ";

        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();
    public function view($fetchMode='ASSOC')
    {
        $STH = $this->DBH->query('SELECT * from city where id='.$this->id);
        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;
    }
    public  function  update()
    {
        $arrData=array($this->name,$this->city);
        $sql="Update city SET name=?,city=? WHERE id=".$this->id ;
        // UPDATE `avijit_kar_atomic_project_b35`.`book_title` SET `author_name` = 'avijit1' WHERE `book_title`.`id` = 3;
        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');
    }

    public function delete(){
        $sql="Delete from city where id=".$this->id;
        // echo $sql;
        // die();
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('index.php');

    }//end of delete();

    public function trash(){

        $sql = "Update city SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()

}// end of BookTitle class